let usersUrl = 'https://ajax.test-danit.com/api/json/users';
let postsUrl = 'https://ajax.test-danit.com/api/json/posts';

class Card {
    constructor(post, user){
        this.post = post;
        this.user = user;
        this.element = this.createCard();
        this.addDeleteListener();
    }

    createCard(){
        let userPost = document.createElement('div');
        userPost.classList.add('user-post');

        let avatar = document.createElement('div');
        avatar.classList.add('avatar');
        avatar.innerHTML = `<img src="./img/no-image.png" alt="no-image" class="avatar-img">`;

        userPost.append(avatar);

        let postInfo = document.createElement('div');
        postInfo.classList.add('post-info');

        let username = document.createElement('div');
        username.classList.add('username');

        let name = document.createElement('span');
        name.classList.add('name');

        let email = document.createElement('span');
        email.classList.add('email');

        let cross = document.createElement('span');
        cross.classList.add('cross-icon');
        cross.innerHTML = '&#10006;';

        postInfo.append(username);

        username.append(name);
        username.append(email);
        username.append(cross);

        let postTitle = document.createElement('div');
        postTitle.classList.add('post-title');

        let postText = document.createElement('div');
        postText.classList.add('post-text');

        postInfo.append(postTitle);
        postInfo.append(postText);

        name.textContent = this.user.name;
        email.textContent = this.user.email;
        postTitle.textContent = this.post.title;
        postText.textContent = this.post.body;
        userPost.append(postInfo);
        return userPost;
    }

    addDeleteListener() {
        const deleteButton = this.element.querySelector('.cross-icon');
        console.log(deleteButton);
        deleteButton.addEventListener('click', () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
                method: 'DELETE'
            })
            .then(response => {
                if (response.ok){
                    this.element.remove();
                }
                else {
                    throw new Error('There is an error with deleting this post!')
                }
            })
            .catch(error => {
                console.log(error.name + ': ' + error.message);
            })
        });
}}

function showNews(users, posts) {
    posts.forEach(post => {
    const user = users.find(user => user.id === post.userId);
    if (user) {
        const card = new Card(post, user);
        document.body.append(card.element);
    }
    });
}

function fetchUsersAndPosts(){
    Promise.all([
        fetch(usersUrl).then(response => response.json()), fetch(postsUrl).then(response => response.json())
    ])
    .then(data => {
        users = data[0];
        posts = data[1];
        console.log(users);
        console.log(posts);
        showNews(users, posts);
    })
    .catch((error) => {
        console.log(error.name + ': ' + 'There is an error with fetching users and posts');
    })
}

fetchUsersAndPosts()